//#include "ESP8266.h"
#include "EmonLib.h"
#include "SoftwareSerial.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <pt.h>
LiquidCrystal_I2C lcd(0x27,16,2);

SoftwareSerial esp(10, 11); //SoftwareSerial pins for MEGA/Uno. For other boards see: https://www.arduino.cc/en/Reference/SoftwareSerial
String SSID     = "KHSFIBR";
String PASSWORD = "0s1l2z3r4";
String server = "192.168.1.43";
String uri = "/store";
const int PORT = 80;

EnergyMonitor emon1;
double sensorValue=0.00;
double sensorValue1=0.00;
int crosscount=0;
int climbhill=0;
double VmaxD=0.00;
double VeffD=0.00;
double Veff=0.00;
double irms = 0.0;
float presentPower=0.00;
float totalPower=0.00;
float prevPower=0.00;
float kWh = 0.00;
float Bill=0.00;
float Rate=9.1924;
static struct pt pt1, pt2;
const int UNIT_ID = 103;

void setup(void)
{
  //Start Serial Monitor at any BaudRate
  Serial.begin(9600);
  esp.begin(115200);
  Serial.println("Begin");
  lcd.init(); //initialize the lcd
  lcd.backlight(); //open the backlight
  lcdPrintStr(0, 0, "System starting up:");
  delay(1000);
  initt();
  emon1.current(1, 65.3);
  lcdPrintStr(0,1,"System Ready");
  PT_INIT(&pt1);
  PT_INIT(&pt2);
}

void initt()
{
  lcdPrintStr(0,1,"WIFI Resetting..");
  reset();
  connectWifi();
}

void reset() {
  esp.println("AT+RST");
  delay(1000);
  if(esp.find("OK") ) {
    Serial.println("Module Reset");
    lcdPrintStr(0,1,"Reset Success");
  }
}

void connectWifi() {
  String cmd = "AT+CWJAP=\"" + SSID + "\",\"" + PASSWORD + "\"";
  esp.println(cmd);
  lcdPrintStr(0,1,"Connecting WIFI");
  delay(4000);
  if(esp.find("OK")) {
    Serial.println("Connected!");
    lcdPrintStr(0,1,"WIFI Connected");
  }
  else {
    Serial.println("Cannot connect to wifi");
    lcdPrintStr(0,1,"Connecting failed");
    connectWifi();
  }
}

static int protothread1(struct pt *pt, int interval) {
  static unsigned long timestamp = 0;
  //static long counterA = 0;
  PT_BEGIN(pt);
  while(1) { // never stop 
    /* each time the function is called the second boolean
    *  argument "millis() - timestamp > interval" is re-evaluated
    *  and if false the function exits after that. */
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval );
    timestamp = millis(); // take a new timestamp
    prevPower = totalPower;
    getPower();
  }
  PT_END(pt);
}

static int protothread2(struct pt *pt, int interval) {
  static unsigned long timestamp = 0;
  //static long counterB = 0;
  PT_BEGIN(pt);
  while(1) {
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval );
    timestamp = millis();
    //sendData();
    httppost();
    //lcd.setCursor(0,1);
    //lcd.print(++counterB);
  }
  PT_END(pt);
}

void loop(void)
{   
    protothread1(&pt1, 1000); // schedule the two protothreads
    protothread2(&pt2, 10000);
}

/*void sendData()
{
    String data("POST /store HTTP/1.1\r\nHost: " + server + "\r\ncontent-type: application/json\r\ncontent-length: " + String(computeBodyLength(Veff, irms, kWh, UNIT_ID)) + "\r\nConnection: close\r\n\r\n{\"voltage\":\"" + String(Veff) + "\",\"current\":\"" + String(irms) + "\",\"kwh\":\"" + String(kWh) + "\",\"unit\":\"" + String(UNIT_ID) + "\"}");
    int len = data.length() + 1;
    char cbody[len];
    data.toCharArray(cbody, len);
    Serial.println(cbody);
    wifi.httpRequest(cbody, server, PORT);
}*/

void httppost () {
  esp.println("AT+CIPSTART=\"TCP\",\"" + server + "\",80");//start a TCP connection.
  if( esp.find("OK")) {
    Serial.println("TCP connection ready");
  }
  long time1 = millis();
  long time2 = time1;
  while((time1 = millis()) - time2 < 1000);
  //delay(1000);
  String data = "{\"voltage\":\"" + String(Veff) + "\",\"current\":\"" + String(irms, 2) + "\",\"kwh\":\"" + String(kWh, 7) + "\",\"unit\":\"" + String(UNIT_ID) + "\"}";
  String postRequest =
    "POST " + uri + " HTTP/1.0\r\n" +
    "Host: " + server + "\r\n" +
    "Accept: *" + "/" + "*\r\n" +
    "Content-Length: " + data.length() + "\r\n" +
    "Content-Type: application/json\r\n" +
    "\r\n" + data;
  String sendCmd = "AT+CIPSEND=";//determine the number of caracters to be sent.
  esp.print(sendCmd);
  esp.println(postRequest.length() );
  //delay(500);
  time1 = millis();
  time2 = time1;
  while((time1 = millis()) - time2 < 500);
  if(esp.find(">")) {
    Serial.println("Sending.."); esp.print(postRequest);
    if( esp.find("SEND OK")) {
      Serial.println("Packet sent");
      while (esp.available()) {
        String tmpResp = esp.readString();
        Serial.println(tmpResp);
      }
      // close the connection
      esp.println("AT+CIPCLOSE");
    }
  }
}

int computeBodyLength(double a, double b, float c, int d)
{
  int lenV = (String(a)).length();
  int lenC = (String(b)).length();
  int lenK = (String(c)).length();
  int lenI = (String(d)).length();
  return(46 + lenV + lenC + lenI + lenK);
}

void lcdPrintStr(int x, int y, String str)
{
  for(int i = 0; i < 16; i++) {
    lcd.setCursor(i,y);
    lcd.print(" ");
  }
  lcd.setCursor(x,y);
  lcd.print(str);
}

void lcdPrintNum(int x, int y, int dec, unsigned long num)
{
  for(int i = 0; i < 16; i++) {
      lcd.setCursor(i,y);
      lcd.print(" ");
  }
  lcd.setCursor(x,y);
  lcd.print(num,dec);
}

void getPower()
{
  sensorValue1=sensorValue;
  sensorValue = analogRead(A0);
  if(sensorValue>sensorValue1 && sensorValue>511)
  {
    climbhill=1;
    VmaxD=sensorValue;
  }
  if(sensorValue<sensorValue1 && climbhill==1)
  {
    climbhill=0;
    VmaxD=sensorValue1;
    VeffD=VmaxD/sqrt(2);
    Veff=(((VeffD-420.76)/-90.24)*-210.2)+370.1;
    //Serial.println(Veff);
    VmaxD=0;
  }
  irms = emon1.calcIrms(1480);  // Calculate Irms only 
  presentPower = Veff*irms;
  totalPower = prevPower + presentPower;
  kWh = (totalPower/3600)/1000;
  Bill = kWh * Rate;
  Serial.print("Current: ");
  Serial.println(irms);             // Irms
  Serial.print("Voltage: ");
  Serial.println(Veff);
  Serial.print("kWh: ");
  Serial.println(kWh,8);
  lcd.setCursor(0,0);
  lcd.print("kWh:");
  lcd.setCursor(4,0);
  lcd.print(kWh,8);
  lcd.setCursor(0,1);
  lcd.print("Bill:");
  lcd.setCursor(5,1);
  lcd.print(Bill,6);
  lcd.setCursor(13,1);
  lcd.print("PHP");
}
