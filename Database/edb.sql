-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 09, 2019 at 11:51 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbelectricity`
--

DROP TABLE IF EXISTS `tbelectricity`;
CREATE TABLE IF NOT EXISTS `tbelectricity` (
  `entryid` int(11) NOT NULL AUTO_INCREMENT,
  `unit` int(11) NOT NULL,
  `voltage` double NOT NULL,
  `current` double NOT NULL,
  `kwh` double NOT NULL,
  `date` date NOT NULL,
  `time` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`entryid`),
  KEY `unit` (`unit`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbelectricity`
--

INSERT INTO `tbelectricity` (`entryid`, `unit`, `voltage`, `current`, `kwh`, `date`, `time`) VALUES
(94, 101, 735, 0, 0, '2019-01-12', ''),
(96, 102, 12, 0, 0, '2019-02-04', '11:36 PM'),
(97, 102, 254.73, 0, 0.02, '2019-02-05', '2:44 PM'),
(98, 103, 254.73, 0.31, 0.0021701, '2019-02-05', '3:12 PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbsessions`
--

DROP TABLE IF EXISTS `tbsessions`;
CREATE TABLE IF NOT EXISTS `tbsessions` (
  `sessionid` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(150) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`sessionid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbsessions`
--

INSERT INTO `tbsessions` (`sessionid`, `token`, `userid`) VALUES
(2, 'ac03dc026447311c302b1f8a9193b924', 1),
(4, '11acc1f9b128d01855cae50635890f5d', 1),
(6, '8df2ebc35f03af5100391a40e7f4881b', 1),
(7, '2c1e21730bb7ada28af154228a9c6483', 1),
(8, '8f73285167b3375328d45e0be398278d', 1),
(9, 'eaeb6a4321af1f1e322ade8fbd192dcf', 1),
(12, 'c9468d51207eb645c1b51cb7ce7c9201', 1),
(17, 'ef178eb976e7d8436f4c0716df467e87', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbunits`
--

DROP TABLE IF EXISTS `tbunits`;
CREATE TABLE IF NOT EXISTS `tbunits` (
  `unitid` int(11) NOT NULL,
  `unitname` char(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `ip` char(15) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`unitid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbunits`
--

INSERT INTO `tbunits` (`unitid`, `unitname`, `active`, `ip`, `userid`) VALUES
(101, 'Room 101', 1, NULL, NULL),
(102, 'Room 102', 1, '192.xxx.xxx.xxx', NULL),
(103, 'Room 103', 1, '192.xxx.xxx.xxx', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

DROP TABLE IF EXISTS `tbusers`;
CREATE TABLE IF NOT EXISTS `tbusers` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `type` char(10) NOT NULL,
  `tenant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`userid`, `username`, `password`, `type`, `tenant`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin'),
(2, 'rufe', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'admin', 'Steven Rufe Padolina'),
(9, 'tenant1', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'tenant', 'Tenant1'),
(10, 'tenant2', 'b9f08bd502a887fe2634e4844aad4a4c83249bad', 'tenant', 'tenant2');

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewunits`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `viewunits`;
CREATE TABLE IF NOT EXISTS `viewunits` (
`unitid` int(11)
,`entryid` int(11)
,`kwh` double
,`voltage` double
,`current` double
,`date` date
,`time` varchar(15)
,`unit` char(50)
);

-- --------------------------------------------------------

--
-- Structure for view `viewunits`
--
DROP TABLE IF EXISTS `viewunits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rufe`@`localhost` SQL SECURITY DEFINER VIEW `viewunits`  AS  select `tbunits`.`unitid` AS `unitid`,`tbelectricity`.`entryid` AS `entryid`,`tbelectricity`.`kwh` AS `kwh`,`tbelectricity`.`voltage` AS `voltage`,`tbelectricity`.`current` AS `current`,`tbelectricity`.`date` AS `date`,`tbelectricity`.`time` AS `time`,`tbunits`.`unitname` AS `unit` from (`tbelectricity` join `tbunits` on((`tbelectricity`.`unit` = `tbunits`.`unitid`))) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbelectricity`
--
ALTER TABLE `tbelectricity`
  ADD CONSTRAINT `unit` FOREIGN KEY (`unit`) REFERENCES `tbunits` (`unitid`);

--
-- Constraints for table `tbsessions`
--
ALTER TABLE `tbsessions`
  ADD CONSTRAINT `tbsessions_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`userid`);

--
-- Constraints for table `tbunits`
--
ALTER TABLE `tbunits`
  ADD CONSTRAINT `userid` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`userid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
