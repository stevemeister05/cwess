const http = require('http');
const mysql = require('mysql');
const StaticServer = require('node-static').Server;
const file = new StaticServer('./public');
const qs = require('querystring');
const sha1 = require('sha1');
const crypto = require("crypto");
const handlers = {};
const conn = mysql.createConnection({ host : "localhost", user : "rufe", password : "abc123", database : "edb" });
conn.connect();

handlers["/"] = (req, res) => {
	file.serve(req, res, (err) => {
		if(err && err.status == 404) {
			returnError("Page not found", res);
		}
	});
};

handlers["/login"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			data.password = sha1(data.password);
			findFromDB("tbusers", data, null, null, (result) => {
				if(result.result.length < 1) {
					res.end(JSON.stringify({ result : "Incorrect username and/or password." }))
				}
				else {
					let sess = {};
					sess.token = crypto.randomBytes(16).toString("hex");
					sess.userid = result.result[0].userid;
					conn.query("INSERT INTO tbsessions SET ?", sess, (error, results, fields2) => {
						if(error) {
							res.end(JSON.stringify({ result : "Login failed. Refresh page and try again." }))
						}
						else {
							res.end(JSON.stringify({ result : "ok", token : sess.token, username : data.username, type : result.result[0].type, userid : result.result[0].userid }));
						}
					});
				}
			});
		});
	}
	else {
		returnError("Invalid request method", res);
	}
};

handlers["/logout"] = (req, res) => {
	if(req.method == "DELETE") {
		let params = qs.parse(req.url);
		let data = {};
		data.token = params["/logout?token"];
		data.userid = params["userid"];
		conn.query("DELETE FROM tbsessions WHERE token = ? AND userid = ?", [data.token, data.userid], (error, results, fields2) => {
			if(error) {
				res.end(JSON.stringify({ result : "Logout failed" }))
			}
			else {
				res.end(JSON.stringify({ result : "ok" }));
			}
		});
	}
	else {
		res.end(JSON.stringify({ result : "Invalid http request method" }))
	}
};

handlers["/checksession"] = (req, res) => {
	if(req.method == "GET") {
		let params = qs.parse(req.url);
		let account = {};
		account.token = params["/checksession?token"];
		account.userid = params["userid"]
		findFromDB("tbsessions", account, null, null, (result) => {
			if(result.result.length < 1) {
				res.end(JSON.stringify({ result : "invalid" }));
			}
			else {
				res.end(JSON.stringify({ result : "ok" }));
			}
		});
	}
	else {
		res.end(JSON.stringify({ result : "Invalid http request method" }))
	}
};

handlers["/setrate"] = (req, res) => {
	if(req.method == "PATCH") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			conn.query("UPDATE tbsettings SET perkwh = ?", [data.perkwh], (error, results, fields2) => {
				if(error) {
					res.end(JSON.stringify({ result : "Something went wrong while updating the rate per kwh. Refresh page and try again." }))
				}
				else {
					res.end(JSON.stringify({ result : "ok" }));
				}
			});
		});
	}
	else {
		returnError("Invalid request method", res);
	}
}

handlers["/setduemonth"] = (req, res) => {
	if(req.method == "PATCH") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			let today = new Date();
			if(data.currentmonth == 11) {
				if(data.duemonth == 0) {
					data.year += 1;
				}
			}
			else if(data.currentmonth == 0) {
				if(data.duemonth == 11) {
					data.year -= 1;
				}
			}
			let duemonth = { duemonth : data.duemonth, year : data.year };
			console.log(data.year);
			findFromDB("tbmonth", duemonth, ["monthid"], null, (result) => {
				if(result.result.length > 0) {
					res.end(JSON.stringify({ result : "This due month is already registered." }));
				}
				else {
					conn.query("UPDATE tbmonth SET duemonth = ?, year = ?", [duemonth.duemonth, duemonth.year], (error, results, fields2) => {
						if(error) {
							res.end(JSON.stringify({ result : "Something went wrong while inserting new due month. Refresh page and try again." }))
						}
						else {
							res.end(JSON.stringify({ result : "ok" }));
						}
					});
				}
			});
			
		});
	}
	else {
		returnError("Invalid request method", res);
	}
}

handlers["/rooms"] = (req, res) => {
	findFromDB("tbunits", null, null, null, (result) => {
		let resp = {};
		resp.data = {};
		resp.data.rooms = result.result;
		findFromDB("tbmonth", null, ["duemonth", "year"], " ORDER BY monthid DESC LIMIT 1", (result) => {
			resp.data.settings = {};
			resp.data.settings.month = result.result[0];
			findFromDB
			findFromDB("tbsettings", null, ["perkwh"], null, (result) => {
				resp.data.settings.rate = result.result[0];
				resp.result = "ok";
				res.end(JSON.stringify(resp));
			});
		});
	});
};

handlers["/room"] = (req, res) => {
	let params = qs.parse(req.url);
	let data = {};
	data.unitid = params["/room?unitid"];
	data.duemonth = params["duemonth"];
	let getRoomDetails = (obj) => {
		findFromDB("viewunits", obj, ["unit", "kwh", "date", "time", "bill"], " ORDER BY entryid DESC LIMIT 1", (result) => {
			let room = {};
			room.current = result.result[0];
			let prsedmonth = data.duemonth.split("-");
			let tmp = parseInt(prsedmonth[0], 10);
			let yr = parseInt(prsedmonth[1], 10);
			let ip1 = (tmp <= 0) ? 11 : tmp - 1;
			if(tmp == 11) {
				if(ip1 == 0) {
					yr += 1;
				}
			}
			else if(tmp == 0) {
				if(ip1 == 11) {
					yr -= 1;
				}
			}
			obj.duemonth = ip1 + "-" + yr;
			findFromDB("viewunits", obj, ["kwh", "bill"], " ORDER BY entryid DESC LIMIT 1", (result) => {
				room.previous = result.result[0];
				res.end(JSON.stringify({ result : "ok", data : room }))
			});
		});
	}
	if(data.unitid) {
		getRoomDetails(data);
	}
	else {
		let dd = {};
		dd.userid = params["/room?userid"];
		findFromDB("tbroomassignment", dd, ["unitid"], null, (result) => {
			if(result.result.length > 0) {
				getRoomDetails(result.result[0]);
			}
			else {
				res.end(JSON.stringify({ result : "You are not assigned to a room yet."}));
			}
		});
	}
};

handlers["/duemonth"] = (req, res) => {
	findFromDB("tbmonth", null, ["duemonth", "year"], null, (result) => {
		res.end(JSON.stringify({result : "ok", data : result.result[0] }));
	});
}

handlers["/users"] = (req, res) => {
	let params = qs.parse(req.url);
	let data = {};
	data.unitid = params["/users?unitid"];
	if(data.unitid) {
		findFromDB("viewusers", { unitid : data.unitid }, ["userid", "tenant"], null, (result) => {
			if(result.result.length) {
				res.end(JSON.stringify({result : "ok", data : result.result }));
			}
			else {
				res.end(JSON.stringify({ result : "no registered tenants found" }));
			}
		});
	}
	else {
		findFromDB("tbusers", { type : "tenant" }, ["userid", "tenant"], null, (result) => {
			if(result.result.length) {
				res.end(JSON.stringify({result : "ok", data : result.result }));
			}
			else {
				res.end(JSON.stringify({ result : "no registered tenants found" }));
			}
		});
	}	
}

handlers["/assigntenant"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			findFromDB("tbroomassignment", { userid : data.userid }, ["id"], null, (result) => {
				if(result.result.length < 1) {
					conn.query("INSERT INTO tbroomassignment SET ?", data, (error, results, fields2) => {
						if(error) {
							res.end(JSON.stringify({ result : "Something went wrong. Please try again." }))
						}
						else {
							res.end(JSON.stringify({ result : "ok" }));
						}
					});
				}
				else {
					res.end(JSON.stringify({ result : "User was not granted with access because it has already an access to a room. Select another user to add access to."}));
				}
			});
			
		});
	}
	else {
		returnError("Incorrect request method for this URL", res);
	}
}

handlers["/accounts"] = (req, res) => {
	let data = null;
	if(req.url.includes("?")) {
		let params = qs.parse(req.url);
		data = { userid : params["/accounts?userid"] };
	}
	findFromDB("tbusers", data, ["userid", "username", "tenant", "type"], null, (result) => {
		if(result.result.length > 0) {
			res.end(JSON.stringify({ result : "ok", data : result.result }));
		}
		else {
			res.end(JSON.stringify({ result : "Something went wrong. Could not fetch account details."}));
		}
	});
}

handlers["/accounts/delete"] = (req, res) => {
	let params = qs.parse(req.url);
	let data = {};
	data.userid = params["/accounts/delete?userid"];
	conn.query("DELETE FROM tbusers WHERE userid = ?", [data.userid], (error, results, fields2) => {
		if(error) {
			res.end(JSON.stringify({ result : "Something went wrong while deleting account. Reload page and try again." }))
		}
		else {
			res.end(JSON.stringify({ result : "ok" }));
		}
	});
}

handlers["/accounts/edit"] = (req, res) => {
	if(req.method == "PATCH") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			conn.query("UPDATE tbusers SET username = ?, tenant = ?, type = ? WHERE userid = ?", [data.username, data.tenant, data.type, data.userid], (error, results, fields2) => {
				if(error) {
					res.end(JSON.stringify({ result : "Something went wrong while updating account. Refresh page and try again." }))
				}
				else {
					res.end(JSON.stringify({ result : "ok" }));
				}
			});
		});
	}
	else {
		returnError("Incorrect request method for this URL", res);
	}
}

handlers["/accounts/add"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (rawdata) => {
			let data = JSON.parse(rawdata);
			let tempData = {};
			tempData.username = data.username;
			findFromDB("tbusers", tempData, ["userid"], null, (result) => {
				if(result.result.length > 0) {
					res.end(JSON.stringify({ result : "Username already exists" }));
				}
				else {
					data.password = sha1("");
					conn.query("INSERT INTO tbusers SET ?", data, (error, results, fields2) => {
						if(error) {
							res.end(JSON.stringify({ result : "Something went wrong while adding new account. Refresh page and try again." }))
						}
						else {
							res.end(JSON.stringify({ result : "ok" }));
						}
					});
				}
			});
			
		});
	}
	else {
		returnError("Incorrect request method for this URL", res);
	}
}

handlers["/changepassword"] = (req, res) => {
	if(req.method == "PATCH") {
		extractData(req, (rawdata) => {
			if(rawdata != "") {
				let data = JSON.parse(rawdata);
				findFromDB("tbusers", { userid : data.userid }, null, null, (result) => {
					if(result.result.length > 0) {
						let currentpassword = result.result[0].password;
						if(sha1(data.password) == currentpassword) {
							conn.query("UPDATE tbusers SET password = ? WHERE userid = ?", [sha1(data.newpassword), data.userid], (error, results, fields2) => {
								if(error) {
									console.log("Insert failed");
									console.log(JSON.stringify(error));
									res.end(JSON.stringify({ result : "FAILED" }))
								}
								else {
									console.log("Insert successful");
									res.end(JSON.stringify({ result : "Password was changed successfully." }));
								}
							});
						}
						else {
							res.end(JSON.stringify({ result : "Incorrect Password" }));
						}
					}
					else {
						res.end(JSON.stringify({ result : "Something went wrong. Can't find room in DB.", data : result.result[0] }));
					}
				});
			}
			else {
				res.end({ result : "Something went wrong. Reload page then try again." });
			}
		});
	}
	else {
		returnError("Incorrect request method for this URL", res);
	}
}

handlers["/store"] = (req, res) => {
	res.writeHead(200, { 'Content-Type' : 'text/json' });
	extractData(req, (rawdata) => {
		if(rawdata != "") {
			let data = JSON.parse(rawdata);
			let today = new Date();
			let dd = today.getDate();
			let mm = today.getMonth() + 1;
			let yyyy = today.getFullYear();
			data.date = "";
			data.date = yyyy + "-" + ((mm < 10) ? "0" + mm : mm) + "-" + ((dd < 10) ? "0" + dd : dd);
			let hour = today.getHours();
			let minute = today.getMinutes();
			let minuteString = (minute > 9) ? minute : "0" + minute;
			let hourString = "";
			if(hour > 12) {
				hourString = hour - 12;
			}
			else if(hour == 0) {
				hourString = 12;
			}
 			else {
				hourString = hour;
			}
			let timeString = ((hour > 12) ? hourString + ":" + minuteString  + " PM" : hourString + ":" + minuteString + " AM");
			data.time = timeString;
			console.log("Date: " + data.date);
			let unit = { unitid : data.unit };
			console.log(rawdata);
			findFromDB("tbmonth", null, ["duemonth", "year"], " ORDER BY monthid DESC LIMIT 1", (result) => {
				if(result.result.length > 0) {
					data.duemonth = result.result[0].duemonth + "-" + result.result[0].year;
					findFromDB("tbunits", unit, null, null, (result) => {
						if(result.result.length < 1) {
							console.log("Not found");
							unit.unitname = "Room " + unit.unitid;
							unit.active = true;
							unit.ip = "192.xxx.xxx.xxx";
							console.log("Inserting unit to database..");
							conn.query("INSERT INTO tbunits SET ?", unit, (error, results, fields2) => {
								if(error) {
									console.log("Insert failed");
									console.log(JSON.stringify(error));
									res.end(JSON.stringify({ response: "FAILED" }));
								}
								else {
									findFromDB("tbelectricity", { date : data.date, unit : unit.unitid, monthid : data.monthid }, null, null, (result) => {
										if(result.result.length < 1) {
											console.log("Inserting entry to database..");
											
											conn.query("INSERT INTO tbelectricity SET ?", data, (error, results, fields2) => {
												if(error) {
													console.log("Insert failed");
													console.log(JSON.stringify(error));
													res.end(JSON.stringify({ response : "FAILED" }))
												}
												else {
													console.log("Insert successful");
													res.end(JSON.stringify({ response : "OK" }));
												}
											});
										}
										else {
											conn.query("UPDATE tbelectricity SET voltage = ?, current = ?, kwh = ?, time = ? WHERE date = ? AND unit = ?", [data.voltage, data.current, data.kwh, data.time, data.date, unit.unitid], (error, results, fields2) => {
												if(error) {
													console.log("Insert failed");
													console.log(JSON.stringify(error));
													res.end(JSON.stringify({ response : "FAILED" }))
												}
												else {
													console.log("Insert successful");
													res.end(JSON.stringify({ response : "OK" }));
												}
											});
										}
									});
								}
								
							});
						}
						else {
							findFromDB("tbelectricity", { date : data.date, unit : unit.unitid, monthid : data.monthid }, null, null, (result) => {
								if(result.result.length < 1) {
									console.log("Inserting entry to database..");
									conn.query("INSERT INTO tbelectricity SET ?", data, (error, results, fields2) => {
										if(error) {
											console.log("Insert failed");
											console.log(JSON.stringify(error));
											res.end(JSON.stringify({ response : "FAILED" }));
										}
										else {
											console.log("Insert successful");
											res.end(JSON.stringify({ response : "OK" }));
										}
									});
								}
								else {
									conn.query("UPDATE tbelectricity SET voltage = ?, current = ?, kwh = ?, time = ? WHERE date = ? AND unit = ?", [data.voltage, data.current, data.kwh, data.time, data.date, unit.unitid], (error, results, fields2) => {
										if(error) {
											console.log("Insert failed");
											console.log(JSON.stringify(error));
											res.end(JSON.stringify({ response : "FAILED" }))
										}
										else {
											console.log("Insert successful");
											res.end(JSON.stringify({ response : "OK" }));
										}
									});
								}
							});
						}
					});
				}
				else {
					//Insert code for inserting present month
				}
			});
			
		}
		else {
			res.end(JSON.stringify({ response : "FAILED" }));
		}
	});
};

var func = (req, res) => {
    console.log(req.url);
	let url = (req.url.includes("?")) ? req.url.split("?")[0] : req.url;
	console.log(url);
	if(handlers[url]) {
		handlers[url](req, res);
	}
	else {
		handlers["/"](req, res);
	}
};

http.createServer(func).listen(80);

let extractData = (req, callback) => {
	let data = "";
	req.on('data', (chunk) => {
		console.log("Extracting data..");
		data += chunk;
	});
	req.on('end', () => {
		console.log("Extracted: " + data);
		callback(data);
	});
};

let findFromDB = (table, obj, qrys, extra, callback) => {
	console.log("Finding from database: " + JSON.stringify(obj));
	let sqlQuery = "";
	if(qrys) {
		sqlQuery = "Select ";
		for(let i = 0; i < qrys.length; i++) {
			sqlQuery  += ((i == 0) ? qrys[i] : ", " + qrys[i]);
		}
		sqlQuery += " FROM ";
	}
	else {
		sqlQuery = "SELECT * FROM ";
	}
	sqlQuery += conn.escapeId(table);
	let inserts = [];
	for(let i = 0; obj && i < Object.keys(obj).length; i++) {
		sqlQuery += (i == 0) ? " WHERE ?? = ?" : " AND ?? = ?";
		inserts.push(Object.keys(obj)[i]);
		inserts.push(Object.values(obj)[i]);
	}
	if(extra) {
		sqlQuery += extra;
	}
	let queryString = mysql.format(sqlQuery + ";", inserts);
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results: ");
			console.log(JSON.stringify(results));
			callback({result: results});
		}
	});
};

let returnError = (msg, res) => {
	res.writeHead(404, { "Content-Type" : "text/html" });
	res.end(msg);
};
