const http = require('http');
const mysql = require('mysql');
const StaticServer = require('node-static').Server;
const file = new StaticServer('./public');
const qs = require('querystring');
const sha1 = require('sha1');
const crypto = require("crypto");
const handlers = {};
const conn = mysql.createConnection({ host : "localhost", user : "rufe", password : "abc123", database : "edb" });
conn.connect();

handlers["/"] = (req, res) => {
	findFromDB("tbmonth", null, ["duemonth", "year"], " ORDER BY monthid DESC LIMIT 1", (result) => {
		if(result.result.length > 0) {
			let a = result.result[0].duemonth + "-" + result.result[0].year
			console.log(a);
		}
	});
};

var func = (req, res) => {
    console.log(req.url);
	let url = (req.url.includes("?")) ? req.url.split("?")[0] : req.url;
	console.log(url);
	if(handlers[url]) {
		handlers[url](req, res);
	}
	else {
		handlers["/"](req, res);
	}
};

http.createServer(func).listen(80);

let findFromDB = (table, obj, qrys, extra, callback) => {
	console.log("Finding from database: " + JSON.stringify(obj));
	let sqlQuery = "";
	if(qrys) {
		sqlQuery = "Select ";
		for(let i = 0; i < qrys.length; i++) {
			sqlQuery  += ((i == 0) ? qrys[i] : ", " + qrys[i]);
		}
		sqlQuery += " FROM ";
	}
	else {
		sqlQuery = "SELECT * FROM ";
	}
	sqlQuery += conn.escapeId(table);
	let inserts = [];
	for(let i = 0; obj && i < Object.keys(obj).length; i++) {
		sqlQuery += (i == 0) ? " WHERE ?? = ?" : " AND ?? = ?";
		inserts.push(Object.keys(obj)[i]);
		inserts.push(Object.values(obj)[i]);
	}
	if(extra) {
		sqlQuery += extra;
	}
	let queryString = mysql.format(sqlQuery + ";", inserts);
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results: ");
			console.log(JSON.stringify(results));
			callback({result: results});
		}
	});
};
