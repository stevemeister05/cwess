let account = JSON.parse(localStorage.getItem("cwess"));
let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

window.onload = function() {
    checkSession();
    let roomsbutton = document.getElementById("roomsbutton");
    let accountsbutton = document.getElementById("accountsbutton");
    let content = document.getElementById("content");
    if(account.type == "tenant") {
        let contentbody = createElement("content", "div", "ccontentbody", null);
        request("GET", "/duemonth", null, (responseText) => {
            let response = JSON.parse(responseText);
            getRoom({ userid : account.userid, duemonth : response.data.duemonth + "-" + response.data.year }, contentbody);
        });
    }
    document.getElementById('menulink').onclick = function() {
        var menu = document.getElementById('menu');
        if(menu.className != 'showmenu') {
            menu.className = 'showmenu';
        }
        else {
            menu.className = 'hiddenmenu';
        }
    };
    document.getElementById("logoutbutton").onclick = () => {
        request("DELETE", "/logout?token=" + account.token + "&userid=" + account.userid, null, (responseText) => {
            let response = JSON.parse(responseText);
            if(response.result == "ok") {
				localStorage.removeItem("cwess");
                window.location.href = "login.html";
            }
            else {
                alert(response.result);
                checkSession();
            }
        });
    };
    changepasswordbutton.onclick = () => {
        hideMenu();
        removeAllChild(content);
        highlightMenuItem(changepasswordbutton);
        let popupcontents = [
            { type : "textbox", ttype : "password", id : "currentpassword", className : "popuptextbox", text : null, placeholder : "Current Password" },
            { type : "textbox", ttype : "password", id : "newpassword1", className : "popuptextbox", text : null, placeholder : "New Password" },
            { type : "textbox", ttype : "password", id : "newpassword2", className : "popuptextbox", text : null, placeholder : "Confirm New Password" },
            { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
            { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
        let changePasswordFunction = () => {
            let pw = document.getElementById("currentpassword");
            let npw = document.getElementById("newpassword1");
            let cpw = document.getElementById("newpassword2");
            if(npw.value != cpw.value) {
                alert("New Passwords don't match");
                npw.value = "";
                cpw.value = "";
            }
            else {
                checkSession();
                request("PATCH", "/changepassword", { userid : account.userid, password : pw.value, newpassword : npw.value }, (responseText) => {
                    let response = JSON.parse(responseText);
                    alert(response.result);
                    if(response.result == "Incorrect Password") {
                        pw.value = "";
                    }
                    else {
                        unhighlightMenuItems();
                        document.body.removeChild(document.getElementById("popupbackground"));
                    }
                });
            }
        }
        let popup = createPopup("Change Password", popupcontents, changePasswordFunction);
    }
    accountsbutton.onclick = () => {
        hideMenu();
        checkSession();
        removeAllChild(content);
        highlightMenuItem(accountsbutton);
        request("GET", "/accounts", null, (responseText) => {
            let response = JSON.parse(responseText);
            if(response.result == "ok") {
                let users = response.data;
                let contentbody = document.getElementById("accountscontentbody");
                if(contentbody && contentbody.innerHTML != "") {
                    contentbody.innerHTML = "";
                }
                else {
                    contentbody = document.createElement("div");
                    contentbody.id = "accountscontentbody";
                }
                content.appendChild(contentbody);
                let accountstableholder = createElement("accountscontentbody", "div", "accountstableholder", null);
                let accountstablewrapper = createElement("accountstableholder", "div", "accountstablewrapper", null);
                let accountstable = createElement("accountstablewrapper", "table", "accountstable", null);
                for(key in users[0]) {
                    let tbheader = createElement("accountstable", "th", null, null);
                    tbheader.appendChild(document.createTextNode(key.toUpperCase()));
                }
                for(user of users) {
                    let tr = createElement("accountstable", "tr", null, "tbrow");
                    for(key in user) {
                        let td = document.createElement("td");
                        td.appendChild(document.createTextNode(user[key]));
                        tr.appendChild(td);
                    }
                    tr.id = user["userid"];
                    tr.onclick = () => {
                        let srows = document.getElementsByClassName("selectedrow");
                        let btn = document.getElementById("accountseditbutton");
                        let btn1 = document.getElementById("accountsdeletebutton");
                        if(srows) {
                            unselectRows();
                            btn.disabled = false;
                            btn1.disabled = false;
                        }
                        else {
                            btn.disabled = true;
                            btn1.disabled = true;
                        }
                        tr.className = "selectedrow";
                        btn.name = tr.id;
                    }
                }
                let unselectRows = () => {
                    for(srow of document.getElementsByClassName("selectedrow")) {
                        srow.className = "tbrow";
                    }
                }
                let accountsaddbutton = createElement(null, "button", "accountsaddbutton", "popupbutton");
                createElement("accountstableholder", "div", null, null).appendChild(accountsaddbutton);
                accountsaddbutton.appendChild(document.createTextNode("Add new account"));
                accountsaddbutton.onclick = () => {
                    let popupcontents = [
                        { type : "textbox", ttype : "text", id : "tbusername", className : "popuptextbox", text : null, placeholder : "Username" },
                        { type : "textbox", ttype : "text", id : "tbtenant", className : "popuptextbox", text : null, placeholder : "Tenant's Name" },
                        { type : "select", id : "stype", className : "popuptextbox", text : null, placeholder : null, options : ["admin", "tenant"]},
                        { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
                        { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                    let addAccountFunction = () => {
                        let uname = document.getElementById("tbusername");
                        let tnt = document.getElementById("tbtenant");
                        let type = document.getElementById("stype");
                        if(uname.value == "" || tnt.value == "") {
                            alert("Please don't leave any empty fields");
                        }
                        else {
                            checkSession();
                            request("POST", "/accounts/add", { username : uname.value, tenant : tnt.value, type : type.value }, (responseText) => {
                                let response = JSON.parse(responseText);
                                if(response.result == "ok") {
                                    unhighlightMenuItems();
                                    document.body.removeChild(document.getElementById("popupbackground"));
                                    accountsbutton.click();
                                }
                                else {
                                    alert(response.result);
                                    if(response.result == "Username already exists") {
                                        document.getElementById("tbusername").value = "";
                                    }
                                }
                            });
                        }
                    }
                    createPopup("New Account", popupcontents, addAccountFunction);
                };
                let accountseditbutton = createElement("accountstableholder", "button", "accountseditbutton", "popupbutton");
                createElement("accountstableholder", "div", null, null).appendChild(accountseditbutton);
                accountseditbutton.appendChild(document.createTextNode("Edit account"));
                accountseditbutton.disabled = true;
                accountseditbutton.onclick = () => {
                    checkSession();
                    request("GET", "/accounts?userid=" + document.getElementById("accountseditbutton").name, null, (responseText) => {
                        let response = JSON.parse(responseText);
                        if(response.result == "ok") {
                            let account = response.data[0];
                            let popupcontents = [
                                { type : "textbox", ttype : "text", id : "tbusername", className : "popuptextbox", text : account.username, placeholder : "Username" },
                                { type : "textbox", ttype : "text", id : "tbtenant", className : "popuptextbox", text : account.tenant, placeholder : "Tenant's Name" },
                                { type : "select", id : "stype", className : "popuptextbox", text : null, placeholder : null, options : [ { text : "admin" }, {text : "tenant" }]},
                                { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
                                { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                            let editAccountFunction = () => {
                                let uname = document.getElementById("tbusername");
                                let tnt = document.getElementById("tbtenant");
                                let type = document.getElementById("stype");
                                if(uname.value == "" || tnt.value == "") {
                                    alert("Please don't leave any empty fields");
                                }
                                else {
                                    request("PATCH", "/accounts/edit", { userid : document.getElementById("accountseditbutton").name, username : uname.value, tenant : tnt.value, type : type.value }, (responseText) => {
                                        let response = JSON.parse(responseText);
                                        if(response.result == "ok") {
                                            unhighlightMenuItems();
                                            document.body.removeChild(document.getElementById("popupbackground"));
                                            accountsbutton.click();
                                        }
                                        else {
                                        alert(response.result);
                                        }
                                    });
                                }
                            }
                            createPopup("Edit Account", popupcontents, editAccountFunction);
                        }
                        else {
                            alert(response.result)
                        }
                    });
                };
                let accountsdeletebutton = createElement("accountstableholder", "button", "accountsdeletebutton", "popupbutton");
                createElement("accountstableholder", "div", null, null).appendChild(accountsdeletebutton);
                accountsdeletebutton.appendChild(document.createTextNode("Delete account"));
                accountsdeletebutton.disabled = true;
                accountsdeletebutton.onclick = () => {
                    let popupcontents = [
                        { type : "p", id : null, className : "popuptext", text : "Are you sure you want", placeholder : null },
                        { type : "p", id : null, className : "popuptext", text : "to delete this account?", placeholder : null },
                        { type : "button", id : "Confirm", className : "popupbutton", text : "Yes", placeholder : null },
                        { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                    let deleteAccountFunction = () => {
                        checkSession();
                        request("DELETE", "/accounts/delete?userid=" + accountseditbutton.name, null, (responseText) => {
                            let response = JSON.parse(responseText);
                            if(response.result == "ok") {
                                document.body.removeChild(document.getElementById("popupbackground"));
                                accountsbutton.click();
                            }
                            else {
                                alert(response.result)
                            }
                        });
                    }
                    createPopup("Confirm Delete", popupcontents, deleteAccountFunction);
                };
            }
            else {
                alert(response.result);
            }
        });
    }
    roomsbutton.onclick = () => {
        hideMenu();
        checkSession();
        removeAllChild(content);
        request("GET", "/rooms", null, (responseText) => {
            let response = JSON.parse(responseText);
            if(response.result == "ok") {
                highlightMenuItem(roomsbutton);
                let roomsdetails = response.data.rooms;
                let sttngs = response.data.settings;
                let sidepanel = document.createElement("div");
                sidepanel.id = "sidepanel";
                content.appendChild(sidepanel);
                let settingsholder = createElement("sidepanel", "div", "settingsholder", null);
                createElement("sidepanel", "div", "infoholder", null);
                let infoheader = createElement("infoholder", "div", "infoheaderholder", "holderrrr");
                infoheader.appendChild(document.createTextNode("Billing Details"));
                createElement("infoholder", "div", "perkwhholder", "holderrrr");
                let perkwhlabel = createElement("perkwhholder", "div", "perkwhlabel", null);
                perkwhlabel.appendChild(document.createTextNode("Rate: "));
                let perkwhbox = createElement("perkwhholder", "div", "perkwhbox", "settingsbox");
                perkwhbox.appendChild(document.createTextNode("P " + sttngs.rate.perkwh + "/kWh"));
                createElement("infoholder", "div", "monthholder", "holderrrr");
                let monthlabel = createElement("monthholder", "div", "monthlabel", null);
                monthlabel.appendChild(document.createTextNode("Month: "));
                let monthbox = createElement("monthholder", "div", "monthbox", "settingsbox");
                monthbox.appendChild(document.createTextNode(months[sttngs.month.duemonth]));
                createElement("infoholder", "div", "yearholder", "holderrrr");
                let yearlabel = createElement("yearholder", "div", "yearlabel", null);
                yearlabel.appendChild(document.createTextNode("Year: "));
                let yearbox = createElement("yearholder", "div", "yearbox", "settingsbox");
                yearbox.appendChild(document.createTextNode(sttngs.month.year));
                let roomsholder = createElement("sidepanel", "div", "roomsholder", null);
                let settings = createElement("settingsholder", "img", "settings", null);
                settings.src = "/files/icons/settings.png";
                settings.onclick = () => {
                    if(document.getElementById("settingsmenu") == null) {
                        let settingsmenu = createElement("sidepanel", "div", "settingsmenu", null);
                        let setrate = createElement("settingsmenu", "div", "setrate", "smenuitem");
                        setrate.appendChild(document.createTextNode("Set Rate"));
                        let changemonth = createElement("settingsmenu", "div", "changemonth", "smenuitem");
                        changemonth.appendChild(document.createTextNode("Change due month"));
                        setrate.onclick = () => {
                            sidepanel.removeChild(settingsmenu);
                            let popupcontents = [
                                { type : "textbox", ttype : "number", id : "tbrate", className : "popuptextbox", text : account.username, placeholder : "Rate/kWh" },
                                { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
                                { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                            let okFunction = () => {
                                let tbrate = document.getElementById("tbrate");
                                if(tbrate.value == 0) {
                                    alert("You must enter a valid rate");
                                }
                                else {
                                    request("PATCH", "/setrate", { perkwh : tbrate.value }, (responseText) => {
                                        let response = JSON.parse(responseText);
                                        if(response.result == "ok") {
                                            document.body.removeChild(document.getElementById("popupbackground"));
                                            roomsbutton.click();
                                        }
                                        else {
                                            alert(response.result);
                                        }
                                    })
                                }
                            }
                            createPopup("Rate", popupcontents, okFunction);
                        }
                        changemonth.onclick = () => {
                            sidepanel.removeChild(settingsmenu);
                            let i = parseInt(sttngs.month.duemonth, 10);
                            let ip1 = (i >= 11) ? 0 : i + 1;
                            let im1 = (i <= 0) ? 11 : i - 1;
                            let temparr = [{ text : months[im1], value : im1 }, { text : months[i], value : i }, { text : months[ip1], value : ip1 }];
                            let popupcontents = [
                                { type : "select", id : "selectmonth", className : "popuptextbox", text : null, placeholder : null, options : temparr},
                                { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
                                { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                            let changemonthfunction = () => {
                                let selectmonth = document.getElementById("selectmonth");
                                if(selectmonth.value != i) {
                                    request("PATCH", "/setduemonth", { currentmonth : i, duemonth : selectmonth.value, year : sttngs.month.year }, (responseText) => {
                                        let response = JSON.parse(responseText);
                                        if(response.result == "ok") {
                                            document.body.removeChild(document.getElementById("popupbackground"));
                                            roomsbutton.click();
                                        }
                                        else {
                                            alert(response.result);
                                        }
                                    });
                                }
                                else {
                                    document.body.removeChild(document.getElementById("popupbackground"));
                                    roomsbutton.click();
                                }
                            }
                            createPopup("Due Month", popupcontents, changemonthfunction);
                            document.getElementById("selectmonth").value = i;
                        }
                    }
                    else {
                        sidepanel.removeChild(document.getElementById("settingsmenu"));
                    }
                }
                for(roomdetails of roomsdetails) {
                    let room = document.createElement("div");
                    room.className = "rooms";
                    room.id = roomdetails.unitid;
                    let roomiconholder = document.createElement("div");
                    roomiconholder.className = "roomiconholder";
                    let roomicon = document.createElement("img");
                    roomicon.className = "roomicon";
                    roomicon.src = "/files/icons/room.png";
                    let roomlabelholder = document.createElement("div");
                    roomlabelholder.className = "roomlabelholder";
                    let roomlabel = document.createElement("h3");
                    roomlabel.className = "roomlabel";
                    let h3 = document.createElement("h3");
                    h3.innerHTML = roomdetails.unitname;
                    roomlabel.appendChild(h3);
                    roomlabelholder.appendChild(roomlabel);
                    roomiconholder.appendChild(roomicon);
                    room.appendChild(roomiconholder);
                    room.appendChild(roomlabelholder);
                    roomsholder.appendChild(room);
                    room.onclick = () => {
                        let contentbody = document.getElementById("contentbody");
                        let settingsmenu = document.getElementById("settingsmenu");
                        if(settingsmenu != null) {
                            sidepanel.removeChild(settingsmenu);
                        }
                        if(contentbody) {
                            if(contentbody.innerHTML != "") {
                                contentbody.innerHTML = "";
                            }
                        }
                        else {
                            contentbody = document.createElement("div");
                            contentbody.id = "contentbody";
                        }
                        getRoom({ unitid : room.id, duemonth : sttngs.month.duemonth + "-" + sttngs.month.year }, contentbody);
                        createElement()
                    }
                }
            }
            else {
                alert(response.result);
            }
        });
    };
};

let getRoom = (room, contentbody) => {
    console.log("getRoom");
    let qrs = "";
    let a = Object.keys(room).length;
    for(let i = 0; i < a; i++) {
        let aa = Object.keys(room)[i] + "=" + Object.values(room)[i]
        aa += (i == a - 1) ? "" : "&";
        qrs += aa;
    }
    request("GET", "/room?" + qrs, null, (responseText) => {
        let response = JSON.parse(responseText);
        if(response.result == "ok") {
            let rdetails = response.data;
            let hpresent = createElement(null, "div", null, "readingheaders");
            contentbody.appendChild(hpresent);
            hpresent.appendChild(document.createTextNode("Present Month Readings:"));
            let data = {};
            if(rdetails.current != null) {
                let date = new Date(rdetails.current.date);
                let day = date.getDate();
                let timeString = rdetails.current.time;
                let datestring = months[date.getMonth()] + " " + ((day < 10) ? "0" + day : day) + ", " + date.getFullYear();
                data = {
                    room : rdetails.current.unit,
                    date : datestring,
                    time : timeString,
                    kwh : rdetails.current.kwh + " kWh",
                    bill : rdetails.current.bill + " pesos"
                };
            }
            else {
                let dataholder = createElement(null, "div", null, "dataholder");
                dataholder.appendChild(document.createTextNode("No record for this month"));
                contentbody.appendChild(dataholder);
            }
            for(key in data) {
                let dataholder = document.createElement("div");
                dataholder.className = "dataholder";
                let p = document.createElement("p");
                let d = document.createTextNode(key.toUpperCase() + ": " + data[key]);
                p.appendChild(d);
                dataholder.appendChild(p);
                contentbody.appendChild(dataholder);
            }
            let hprevious = createElement(null, "div", null, "readingheaders");
            contentbody.appendChild(hprevious);
            hprevious.appendChild(document.createTextNode("Previous Month Readings:"));
            if(rdetails.previous) {
                let pdata = {
                    kwh : rdetails.previous.kwh + " kWh",
                    bill : rdetails.previous.bill + " pesos"
                };
                for(key in pdata) {
                    let dataholder = document.createElement("div");
                    dataholder.className = "dataholder";
                    let p = document.createElement("p");
                    let d = document.createTextNode(key.toUpperCase() + ": " + pdata[key]);
                    p.appendChild(d);
                    dataholder.appendChild(p);
                    contentbody.appendChild(dataholder);
                }
            }
            else {
                let dataholder = createElement(null, "div", null, "dataholder");
                dataholder.appendChild(document.createTextNode("No record for this the previous month"));
                contentbody.appendChild(dataholder);
            }
            if(account.type == "admin") {
                request("GET", "/users?unitid=" + room.unitid, null, (responseText) => {
                    let response = JSON.parse(responseText);

                    let hprevious = createElement(null, "div", null, "readingheaders");
                    hprevious.appendChild(document.createTextNode("Tenants:"));
                    contentbody.appendChild(hprevious);
                    let tenantholder = createElement(null, "div", "tenantholder", null);
                    contentbody.appendChild(tenantholder);
                    createElement("tenantholder", "div", "tenantwrapper", null);
                    createElement("tenantwrapper", "div", "usertablewrapper", null);
                    let usertable = createElement("usertablewrapper", "table", "usertable", null);
                    let bholder = createElement("tenantwrapper", "div", null, "bholder");
                    let assignbutton = createElement(null, "button", "assignbutton", "tenantbutton");
                    assignbutton.appendChild(document.createTextNode("Assign User Access"));
                    bholder.appendChild(assignbutton);
                    assignbutton.onclick = () => {
                        request("GET", "/users", null, (responseText) => {
                            let response = JSON.parse(responseText);
                            if(response.result == "ok") {
                                let temparr = [];
                                for(user of response.data) {
                                    temparr.push({ value : user.userid, text : user.tenant });
                                }
                                let popupcontents = [
                                    { type : "select", id : "selecttenant", className : "popuptextbox", text : null, placeholder : null, options : temparr},
                                    { type : "button", id : "Confirm", className : "popupbutton", text : "Confirm", placeholder : null },
                                    { type : "button", id : "Cancel", className : "popupbutton", text : "Cancel", placeholder : null }];
                                let assigntenantfunction = () => {
                                    let selecttenant = document.getElementById("selecttenant");
                                    request("POST", "/assigntenant", { userid : selecttenant.value, unitid : room.unitid }, (responseText) => {
                                        let response = JSON.parse(responseText);
                                        if(response.result == "ok") {
                                            document.body.removeChild(document.getElementById("popupbackground"));
                                            document.getElementById(room.unitid).click();
                                        }
                                        else {
                                            alert(response.result);
                                        }

                                    });
                                }
                                createPopup("Assign Tenant to Room", popupcontents, assigntenantfunction);
                            }
                            else {
                                alert(response.result);
                            }
                        });
                    }
                    if(response.result == "ok") {
                        let users = response.data;
                        for(key in users[0]) {
                            let tbheader = createElement("usertable", "th", null, null);
                            tbheader.appendChild(document.createTextNode(key.toUpperCase()));
                        }
                        for(user of users) {
                            let tr = createElement("usertable", "tr", null, "tbrow");
                            for(key in user) {
                                let td = document.createElement("td");
                                td.appendChild(document.createTextNode(user[key]));
                                tr.appendChild(td);
                            }
                            tr.id = user["userid"];
                            tr.onclick = () => {
                                let srows = document.getElementsByClassName("selectedrow");
                                if(srows) {
                                    unselectRows();
                                }
                                tr.className = "selectedrow";
                                //btn.name = tr.id;
                            }
                            tr.ondblclick = () => {
                                
                                //Insert "Delete" code

                            }
                        }
                        let unselectRows = () => {
                            for(srow of document.getElementsByClassName("selectedrow")) {
                                srow.className = "tbrow";
                            }
                        }
                    }
                });
                
            }
            document.getElementById("content").appendChild(contentbody);
        }
        else {
            let popupcontents = [
                { type : "p", id : null, className : "popuptext", text : response.result, placeholder : null },
                { type : "button", id : "Confirm", className : "popupbutton", text : "Ok", placeholder : null }];
            let okFunction = () => {
                document.body.removeChild(document.getElementById("popupbackground"));
            }
            createPopup("Error", popupcontents, okFunction);
        }
    });
};

let request = (method, url, obj, callback) => {
	let rq = new XMLHttpRequest();
	rq.onreadystatechange = function() {
		if(rq.readyState == 4) {
			console.log("response url: " + rq.responseURL);
			if(callback) {
				callback(rq.responseText);
			}
		}
	};
	rq.open(method, url, true);
	if(obj != null) {
		rq.send(JSON.stringify(obj));
	}
	else {
		rq.send();
	}
};

let checkSession = () => {
    if(account != null && account.username != null) {
        request("GET", "/checksession?token=" + account.token + "&userid=" + account.userid, null, (responseText) => {
            let response = JSON.parse(responseText);
            if(response.result != "ok") {
                alert("Session has expired.");
                window.location.href = "login.html";
				localStorage.removeItem("cwess");
            }
            else {
                if(account.type == "tenant") {
                    document.getElementById("roomsbutton").style.display = "none";
                    document.getElementById("accountsbutton").style.display = "none";
                }
            }
        });
    }
    else {
        window.location.href = "login.html";
    }
};

let hideMenu = () => {
    document.getElementById("menu").className = "hiddenmenu";
};

function highlightMenuItem(menuitem) {
	let a = document.getElementsByClassName("selectedmenuitem");
	if(a[0]) {
		a[0].className = "menuitem";
	}
	menuitem.className = "selectedmenuitem";
};

function unhighlightMenuItems() {
    let menuitems = document.getElementsByClassName("selectedmenuitem");
    for(menuitem of menuitems) {
        menuitem.className = "menuitem";
    }
}

function removeAllChild(parent) {
	parent.innerHTML = "";
}

let createPopup = (headerString, popupContents, popupFunction) => {
    let popupbackground = document.createElement("div");
    document.body.appendChild(popupbackground);
    popupbackground.id = "popupbackground";
    createElement("popupbackground", "div", "popupholder", null);
    createElement("popupholder", "div", "popupwrapper", null);
    createElement("popupwrapper", "div", "popup", null);
    createElement("popup", "div", "popupheader", null).innerHTML = headerString;
    for(content of popupContents) {
        let tempdiv = createElement("popup", "div", null, null);
        switch(content.type) {
            case "textbox":
                let tb = createElement(null, "input", content.id, content.className);
                tb.type = content.ttype;
                tb.value = content.text;
                tb.placeholder = content.placeholder;
                tempdiv.appendChild(tb);
                break;
            case "button":
                let bt = createElement(null, "button", content.id, content.className);
                bt.appendChild(document.createTextNode(content.text));
                if(content.text.toLowerCase() == "cancel") {
                    bt.onclick = () => {
                        document.body.removeChild(popupbackground);
                        unhighlightMenuItems();
                    }
                }
                else {
                    bt.onclick = popupFunction;
                }
                tempdiv.appendChild(bt);
                break;
            case "select":
                let slct = createElement(null, "select", content.id, content.className);
                for(opt of content.options) {
                    let op = createElement(null, "option", null, null);
                    op.appendChild(document.createTextNode(opt.text));
                    if(opt.value != null) {
                        op.value = opt.value;
                    }
                    slct.appendChild(op);
                    tempdiv.appendChild(slct);
                }
                break;
            case "p":
                let p = createElement("popup", content.type, content.id, content.className);
                p.appendChild(document.createTextNode(content.text));
                tempdiv.appendChild(p);
                break;
            default:
                let el = createElement("popup", content.type, content.id, content.className);
                el.text = content.text;
                tempdiv.appendChild(el);
                break;
        }
    }
}

function createElement(parentid, type, id, classname) {
	let parent = document.getElementById(parentid);
	let element = document.createElement(type);
	if(id) {
		element.id = id;
	}
	if(classname) {
		element.className = classname;
    }
    if(parentid) {
	    parent.appendChild(element);      
    }
	return(element);
}
